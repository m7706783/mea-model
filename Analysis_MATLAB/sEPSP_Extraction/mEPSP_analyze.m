%This script can be used to analyze voltage clamp traces containing sEPSC.
% WRITTEN BY: Nina Doorn (11-07-2022)
%It will return a matrix with every row one of the cells/traces you
%analyzed, and the columns:
% -number of the run
% -average sEPSC amplitude
% -sEPSC frequency 
% -The threshold used for event detection
% -The length of the signal used (time in seconds)
% -The maximum amplitude of the signal (more negative if network bursts (NB))
% -The root mean square as a measure for the noisynes/amount of the NBs

clear all; close all; clc

%Select where the files are 
inputpath = 'mEPSPdata/26_05_22/26_05_22' ;

%choose parameters:
filtlow = 5;                                    %lower cutoff bandpass filter 
filthigh = 1000;                                %high cutoff bandpass filter
maxamp = 100;                                   %maximal amplitude of a EPSC (pA)                             

%Initialize  
files = dir(fullfile(inputpath, '*.abf')) ;     %collects all the files in the input folder
outputmat = zeros(length(files), 7);            %initiates the matrix where output parameters will be saved
outputtraces = zeros(length(files), 201);
n=0;
p=1;

%analyze 
for file = files'                               %iterates over the files 
    n = n+1;
    disp(append('processing file', file.name))
    
    filepath = append(inputpath, '/',  file.name);
    [d_unfilt, si, h] = abfload2(filepath);     %reads the abf file
    dt = si/1000000;                            %timestep
    T = h.lActualAcqLength/10000;               %length of the measurement
    t = dt:dt:T;                                %makes a timevector
    fs = 1/dt ;                                 %sampling frequency
    mindist = 0.4*fs;                           %minimal time between two EPSC (second)

    %filter the signal 
    [b,a] = butter(2, [filtlow filthigh]/(fs/2));
    d = filter(b,a,d_unfilt);
    
    %plot timeseries to see if it is ok
    figure()
    hold on
    plot(t,d)
    xlabel('time(second)')
    ylabel('trace (pA)')
    title('Trace to be analyzed')
    drawnow
    
    %asks the used if the signal is usable yes (Y) or no (N)
    prompt = "Is the signal ok? Y/N [Y]: ";     %if N (no), the signal will be disregarded
    txt = input(prompt,"s");
    if isempty(txt)
        txt = 'Y';
    end
    
    if txt ~= 'Y'
        continue 
    end 

    noise = rms(d);
    
    %specify where to stop and start the signal 
    disp('select a start point of the signal');
    [x1,~] = ginput(1);
    disp('select where to stop analyzing the signal')
    [x2,~] = ginput(1);
    start = round(x1*fs);
    stop = round(x2*fs);
    d = d(start:stop);                                  %only take the signal selected 
    t = t(start:stop);
    T = t(end)-t(1);                                    %new length of the signal
    
    close all 
    
    %plot and determine the threshold
    hold on 
    plot(d)
    yline(-10, '--');
    ylabel('trace (pA)')
    title('Filtered trace to be analyzed')
    drawnow
    
    %asks the user what the threshold should be. Note: it's a negative
    %threshold but should be given as a positive values. So threshold = 10
    %mV means everything below -10 mV can be taken as EPSC. 
    prompt = 'what should the threshold value for EPSC detection be? (in positive mV)';
    threshold = input(prompt);
    
    %detect EPSCs based on threshold, maxamp, and mindist
    [pks, locs] = findpeaks(-d, 'MinPeakHeight', threshold, 'MinPeakDistance', mindist);
    
    %exlude events in NBs 
    NBlocs = pks>maxamp;                     
    pksnoNB = pks(~NBlocs);
    locsnoNB = locs(~NBlocs);
    
    %show the detected peaks
    plot(locsnoNB, -pksnoNB, 'x', 'markersize', 6)
    drawnow
    hold off 

    %ask the user if the peaks are detected properly 
    prompt = 'are the peaks detected properly? Y/N [Y]: ';
    txt = input(prompt,"s");
    if isempty(txt)
        txt = 'Y';
    end
    
    if txt ~= 'Y'
        close all
        continue 
    end 
    
    %save the shapes to later make an average 
    mEPSPshapes = zeros(1,201); 
    for temp = 1:length(locsnoNB)
        mEPSPshapes(p,:) = -d(locsnoNB(temp)-100:locsnoNB(temp)+100);
        p = p+1;
    end 
    outputtraces(n,:) = mean(mEPSPshapes); 

    %out output in a matrix 
    outputmat(n,:) = [n, mean(pksnoNB), length(locsnoNB)/T, threshold, T ,min(d), noise];
    close all;
end 

%save the output and write to excell file. 
save('mEPSPdata/output/output19_05_2022.mat', 'outputmat')
outputmat2 = outputmat(:,2:end);
xlswrite('mEPSPdata/output/output19_2',outputmat2)
