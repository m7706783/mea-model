%%% Script to compute the output measures of the Computational Model Drave article of the experimental data. 
% It takes raw data files in .mat format, goes through the wells, detects spikes at every electrode, 
% computed bursts and then computes the related output measures

%%% Nina Doorn - n.doorn-1@utwente.nl   - 14-10-2022

load('MK801_block.mat')                                                     %choose file you want to analyze
outputmat = zeros(6, 3);                                                    %initiates the matrix where output parameters will be saved
for n=1:6                                                                   %iterates through the wells
    clearvars -except n files file outputmat MK801_block
    close all

    disp(append('processing well', num2str(n)))
    
    well = n;                           
    factor = 4;                                                             % threshold for AP detection (set same as simulation!)
    MEA_Signals = double(MK801_block(:,(well-1)*12+1:(well-1)*12+12).');    % selects all channels within one well
    
    fs=10000;                                                               %sampling frequency 
    MEA_filtered=[];
    
    %%filtering the signal from 100 to 3500 Hz
    for i = 1:12
        readrawd=MEA_Signals(i,:)-mean(MEA_Signals(i,:));
        fnyq= 0.5*fs;                                                       %nyquist frequency
        fc1= 3500;                                                          %continuous cut of frequency (low pass) 
        fc2= 100;                                                           %continuous cut of frequency (high pass)
    
        nfc1=fc1/fnyq;                                                      %normalized freq: nfc=fc/fnyq
        nfc2=fc2/fnyq;
    
        N=2;                                                                %order of the filter
        [Bl,Al]=butter(4,nfc1);                                             %low pass
        [Bh,Ah]=butter(N,nfc2,'high');                                      %high pass
    
    
        %zero-phase digital filtering in both the forward and reverse directions
        rawlow=filter(Bl,Al,readrawd);                          
        rawdh=filter(Bh,Ah,(rawlow));   
        MEA_filtered=[MEA_filtered ;rawdh]; 
    end
    
    %%Detect action potentials with threshold based method
    Ts_AP = [0, 0];
    for i=1:12
        tempsig = MEA_filtered(i,:);
        noise = rms(tempsig);                                               %determines the noise ratio
        thres = factor * noise;                                             %set the threshold for AP detection
        [~, Ts]= findpeaks(abs(tempsig), 'minPeakHeight', thres);           %detects APs
        Ts_AP = [Ts_AP; [i*ones(length(Ts),1), Ts']];                       %Saves them and accompanying electrode number 
    end 
    
    Ts_AP = Ts_AP(2:end,:);                                                 %remove the first row with zeros 

    %Detect Network bursts based on total spike rate
    load('MEA_Timestamps.mat')                                              %load the timestamps
    Ts_AP2  = sort(Ts_AP(:,2));                                             %sort the APs based on time
    binsize = 20e-3;                                                        %Time window to compute timestamps
    
    %compute spikerate
    for j=1:12                                                              %loop over the electrodes                                                       
        x = Ts_AP(Ts_AP(:,1) == j,:);
        x2 = x(:,2);
        APtimes = MEA_Timestamps(x2);                                       %find the times of the action potentials 
    
        %Compute the spike rate per timebin
        for k=1:floor(600/binsize)
            APsinbins(j,k) = sum(APtimes>((k-1)*binsize) & APtimes<((k-1)*binsize+binsize));
            rate(j,k) = APsinbins(j,k)/60/binsize;
        end 
    end
    
    APinbinstot = sum(APsinbins, 1);
    ratetot = sum(rate, 1);                                                 %sum the rate of the individual electrodes 
    ratetimestamps = MEA_Timestamps(1:(binsize*fs):end);                    %for plotting
    
    %%plot the spike rate to later check the burst rate
    figure()
    plot(ratetimestamps, ratetot);
    title('Total Spike Rate')
    xlabel('Time (s)')
    
    %%Detect Network bursts 
    thr1 = (1/4)*max(ratetot);                                              %threshold for burst start
    thr2 = (1/100)*max(ratetot);                                            %threshold for burst stop
    NBcount = 1;
    maxNB = 1000;
    i=1;
    bursts = zeros(maxNB, 3);
    
    while (i+2)<length(ratetot)                                             %loop through all timebins
        if all(ratetot(i:i+2)>thr1)                                         %if three time bins are above first threshold, start burst
            bursts(NBcount,3) = bursts(NBcount, 3) + sum(APinbinstot(i:i+2));
            bursts(NBcount,1) = i; 
            i=i+3;
            while ratetot(i) >  thr2                                        %stop burst as soon as the spikerate drops below second threshold
                bursts(NBcount, 3) = bursts(NBcount, 3) + APinbinstot(i);
                i = i+1;
                if i >length(ratetot)
                    break 
                end 
            end  
            if i < 30001
                bursts(NBcount,2) = i; 
            else 
                bursts(NBcount,2) = 30000;
            end 
            NBcount = NBcount + 1;  
            if i >length(ratetot)
                break 
            end 
            i=i+1;
        else 
            if i >length(ratetot)
                break 
            end 
            i=i+1;
        end   
    end 
    
    bursts= bursts(bursts(:,1)~=0,:);                                       %Delete the empty rows
    
    %%Perform a check to see the bursts are not just at one electrode
    Ratedist = zeros(length(bursts), 12);
    for i=1:length(bursts(:,1))
        for j=1:12
            Ratedist(i,j) = sum(rate(j,bursts(i,1):bursts(i,2)))/sum(ratetot(bursts(i,1):bursts(i,2)));
        end 
        if any((Ratedist(i,:)) > 0.8)
            disp(['Burst number ', num2str(i), ' appears to be made up of only one electrode and is thus not valid'])
            bursts(i,:) = [];
        end 
    end 
    
    %%Compute and display measures         
    MNBR = (NBcount-1) * (60 / (round(MEA_Timestamps(end))));
    NBDs = (bursts(:,2)-bursts(:,1))*binsize;
    MNBD = mean(NBDs);
    PSIB = sum(bursts(:,3))/length(Ts_AP2)*100;
    disp(['Mean Network Burst Rate is ', num2str(MNBR)])
    disp(['Mean Network Burst Duration is ', num2str(MNBD)])
    disp(['Percentage of spikes in burst is ', num2str(PSIB), '%'])
    outputmat(n,:) = [MNBR, MNBD, PSIB];

end 

% write the output measures to a file
xlswrite('MK801_experimentaloutput_block',outputmat)