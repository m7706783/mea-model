%%% Script to compute the output measures of the Computational Model Drave article of the simulated. 
% It takes all data files in a folder that are .mat format and containing electrode numbers (first column) 
% and spiketimes (second column). It detects network bursts based on spike
% rate, exactly the same as for experimental OMs, and computes the output
% measures. 

%specify where the simulation outputs are you want to iterate over. 
files = dir(fullfile('C:\Users\DoornN\OneDrive - University of Twente\Documenten\Universiteit\PhD\Papers\Dravet_paper\Simulated_data\Validation\output*')) ;     %collects all the files in the input folder
outputmat = zeros(length(files), 3);                                        %initiates the matrix where output parameters will be saved
n=0;
for file = files'                                                           %iterates over the files
    clearvars -except n files file outputmat
    close all
    n = n+1;

    disp(append('processing file', file.name))
    load("C:\Users\DoornN\OneDrive - University of Twente\Documenten\Universiteit\PhD\Papers\Dravet_paper\Simulated_data\Validation\" + file.name)
    Ts_AP = double(APs);
    Ts_AP = Ts_AP(2:end,:);
    Ts_AP(:,1) = Ts_AP(:,1)+1;                                              %python index starts at 0, matlab at 1

    %Omit the transient (first 50 seconds of data). 
    plcs = Ts_AP(:,2) > 500000;
    Ts_AP = Ts_AP(plcs, :); 
    Ts_AP(:,2) = Ts_AP(:,2) - 500000;
    label                                                                   %Check if it is the simulation you want
    fs=10000;                                                               %sample frequency 
    load('MEA_Timestamps.mat')
    Ts_AP2  = sort(Ts_AP(:,2));
    APtimes = MEA_Timestamps(Ts_AP2);
    binsize = 20e-3;                                                        %bin size to compute spike rate 
    
    for j=1:12
        x = Ts_AP(Ts_AP(:,1) == j,:);
        x2 = x(:,2);
        APtimes = MEA_Timestamps(x2);                                       %find the times of the action potentials 
    
        %Compute the spike rate per bin of 10 milliseconds 
        for k=1:floor(600/binsize)
            APsinbins(j,k) = sum(APtimes>((k-1)*binsize) & APtimes<((k-1)*binsize+binsize));
            rate(j,k) = APsinbins(j,k)/60/binsize;
        end 
    end
    
    APinbinstot = sum(APsinbins, 1);
    ratetot = sum(rate, 1);                                                 %sum the rate of the individual electrodes 
    ratetimestamps = MEA_Timestamps(1:(binsize*fs):end);                    %for plotting
    
    %%plot the spike rate to later check the burst rate
    figure()
    plot(ratetimestamps, ratetot);
    title('Total Spike Rate')
    xlabel('Time (s)')
    
    %%Detect Network bursts 
    thr1 = (1/4)*max(ratetot);                                              %threshold for burst start
    thr2 = (1/100)*max(ratetot);                                            %threshold for burst stop
    NBcount = 1;
    maxNB = 1000;
    i=1;
    bursts = zeros(maxNB, 3);
    
    while (i+2)<length(ratetot)
        if all(ratetot(i:i+2)>thr1)
            bursts(NBcount,3) = bursts(NBcount, 3) + sum(APinbinstot(i:i+2));
            bursts(NBcount,1) = i; 
            i=i+3;
            while ratetot(i) >  thr2
                bursts(NBcount, 3) = bursts(NBcount, 3) + APinbinstot(i);
                i = i+1;
                if i >length(ratetot)
                    break 
                end 
            end  
            if i < 30001
                bursts(NBcount,2) = i; 
            else 
                bursts(NBcount,2) = 30000;
            end 
            NBcount = NBcount + 1;  
            if i >length(ratetot)
                break 
            end 
            i=i+1;
        else 
            if i >length(ratetot)
                break 
            end 
            i=i+1;
        end   
    end 
    
    bursts= bursts(bursts(:,1)~=0,:);                                       %Delete the empty rows
    
    %%Perform a check to see the bursts are not just at one electrode
    Ratedist = zeros(length(bursts), 12);
    for i=1:length(bursts(:,1))
        for j=1:12
            Ratedist(i,j) = sum(rate(j,bursts(i,1):bursts(i,2)))/sum(ratetot(bursts(i,1):bursts(i,2)));
        end 
        if any((Ratedist(i,:)) > 0.8)
            disp(['Burst number ', num2str(i), ' appears to be made up of only one electrode and is thus not valid'])
            bursts(i,:) = [];
        end 
    end 
    
    %%Compute and display measures         
    MNBR = (NBcount-1) * (60 / (round(MEA_Timestamps(end))));
    NBDs = (bursts(:,2)-bursts(:,1))*binsize;
    MNBD = mean(NBDs);
    PSIB = sum(bursts(:,3))/length(Ts_AP2)*100;
    disp(['Mean Network Burst Rate is ', num2str(MNBR)])
    disp(['Mean Network Burst Duration is ', num2str(MNBD)])
    disp(['Percentage of spikes in burst is ', num2str(PSIB), '%'])
    outputmat(n,:) = [MNBR, MNBD, PSIB];

end 

%write the outpute measures to an excell file. 
xlswrite('NMDAblock_output',outputmat)
 