# Model of neuronal network derived from hiPSCs on MEA
# Using a HH-type model including an after-hyperpolarization current and membrane noise.
# Connected into a network with N neurons, with choice of topology.
# Connected via AMPAr and NMDAr mediated synapses, subjected to short-term plasticity.
# Choice of distributed weights and distant dependent conduction delays.
# Virtual electrodes are placed to mimic experimental data
# All settings used for the Control model are specified. DS model properties are mentioned in comments

# Written by Nina Doorn
# last edit: 26-07-2022
# n.doorn-1@utwente.nl

# IMPORT LIBRARIES
from brian2 import devices
import scipy
from scipy import signal
import os
from numpy import *
from numpy import random
from brian2 import *
import networkx as nx
import matplotlib.pyplot as plt
import pickle

from scipy.signal import find_peaks
from scipy.io import savemat

# # Skip C99 support check
import brian2.codegen.cpp_prefs
brian2.codegen.cpp_prefs._compiler_supports_c99 = True

# import brian2cuda
# set_device("cuda_standalone")

BrianLogger.suppress_name('floating_point_division')
BrianLogger.suppress_name('shape')

# SET PARAMETERS
# where to save output
outputdir = '/home/Nina/Documents/M_Thesis/DS_project/Output'
simname = '/Test'

# choose with plots you want to see
voltplot = True                     # plots the membrane potential of one neuron in the network over time
electrodeplot = True                # plots the voltage measured by the 12 virtual electrodes
rasterplot = True                   # Makes a rasterplot with all the neurons
rasterlecplot = True                # Makes a rasterplot of APs detected at electrodes (electrodeplot should be True)
STDplot = False                     # Plots short-term depression parameters over time
synapseplot = True                  # Plots AMPA and NMDA currents
adaptationplot = True               # Plots the afterhyperpolarizatoin current over time
topologyplot = False                # Draws the graph of the network
topologyelecplot = False            # Draws the graph of the network with the electrode placement
spikerateplot = True                # Plots total spikerate over time and burst detection thresholds
singleelecplot = True               # Plots the voltage measured at a single electrode

# network parameters
Nl = 10
N = Nl * Nl                         # number of neurons
simtime = 60 * second               # simulation time
transient = 5 * second              # transient not considered for data anlysis
devices.device.seed(39)             # set the seed for all the random number realisations

# choose topology
smallworld = False                  # Small world topology
sparseconnectivity = True           # Random sparse connectivity with connection probability prob
prob = 0.3
fullconnectivity = False            # All neurons are connected to all neurons
distancedependance = False          # If neurons are connected depends on the distance, with
alpha = 0.025                       # overal change of connectivity alpha

distributed = True                  # Choose if the synaptic weights are normally distributed with standard deviation sd
sd = 0.7
DistDelays = True                   # Choose if the delays should be distant dependent
Maxdelay = 25*ms                    # with maximum conduction delay

# neuron parameters
T = 37                              # temperature
area = 300*umetre**2
Cm = (1*ufarad*cm**-2) * area       # membrane capacitance
El = -39.2 * mV                     # Nernst potential of leaky ions
EK = -80 * mV                       # Nernst potential of potassium
ENa = 70 * mV                       # Nernst potential of sodium
g_na = (50*msiemens*cm**-2) * area  # maximal conductance of sodium channels
g_kd = (5*msiemens*cm**-2) * area   # maximal conductance of potassium
gl = (0.3*msiemens*cm**-2) * area   # maximal leak conductance
VT = -30.4*mV                       # alters firing threshold of neurons
sigma = 4.1 * mV                    # standard deviation of the noisy voltage fluctuations

#temperature parameters
phi = 3**((T-37)/10)
B = 0                               # set to 0.05853 to include temperature dependence on conductances
eta = 1+B*(T-37)

# Adaptation parameters
Adaptation = True                   # Add spike-frequency adaptation using afterhyperolarization current
E_AHP = EK                          # Nernst potential of afterhyperpolarization current
g_AHP = 10 * nS                     # 10 nS maximum conductance of AHP channels
tau_Ca = 6000 * ms                  # recovery time constant AHP channels
if Adaptation:
    alpha_Ca = 0.00035              # strength of the spike-frequency adaptation --DS MODEL VALUE: 0.00005
else:
    alpha_Ca = 0

# synapse parameters
delta = 0                           # increase to alter NMDAR/AMPAR ratio
g_ampa = ((104.0 * (1 + 10 * delta)) / 1000) * 2.7 * nS  # maximal conductance of AMPA channels
g_nmda = ((327 * (1 - delta)) / 1000) * 0.3 * nS
E_ampa = 0 * mV                     # Nernst potentials of synaptic channels
E_nmda = 0 * mV
tau_ampa = 2 * ms                   # recovery time constant of ampa conductance
taus_nmda = 100 * ms                # decay time constant of nmda conductance
taux_nmda = 2 * ms                  # rise time constant of nmda conductance
alpha_nmda = 0.5 * kHz

S = 1.65                            # weight scaling parameter -- DS MODEL VALUE: 0.7

#Short-Term Depression parameters
tau_d = 813 * ms                    # Recovery time constant of synaptic depression
U = 0.015                           # Magnitude of depression -- DS MODEL VALUE: 0.01

# sodium channel modification parameters
gam_na = 1                          # modulates maximal conductance
gam_tm = 1                          # modulates both rate constants of m gate -- DS MODEL VALUE: 0.8
gam_th = 1                          # modulates both rate constants of h gate
gam_am = 1                          # modulates activation rate of m gate
gam_bm = 1                          # modulates deactivation of m gate
gam_ah = 1                          # modulates activation of h gate
gam_bh = 1                          # modulates deactivation of h gate
VSm = 0*mV                          # shifts voltage sensitivity of rate constants of m gate
VSh = 0*mV                          # shifts voltage sensitivity of rate constants of h gate

gam_nap = 0                         # modulates what fraction (0-1) of sodium channels become persistent

max_g_nap = (0*msiemens*cm**-2) * area  #maximal persistent sodium conductance
g_nap = gam_nap*max_g_nap
gam_na = (1-gam_nap)*gam_na

# BUILD NETWORK
# neuron model
eqs = Equations('''
dV/dt = noise + (-gl*eta*(V-El)-gam_na*g_na*eta*(m*m*m)*h*(V-ENa)-g_kd*eta*(n*n*n*n)*(V-EK)+I-I_syn+I_AHP+I_Nap)/Cm : volt
dm/dt = phi*gam_tm*(gam_am*alpha_m*(1-m)-gam_bm*beta_m*m) : 1
dh/dt = phi*gam_th*(gam_ah*alpha_h*(1-h)-gam_bh*beta_h*h) : 1
dn/dt = phi*(alpha_n*(1-n)-beta_n*n) : 1
dhp/dt = 0.128*exp((17.*mV-V+VT)/(18.*mV))/ms*(1.-hp)-4./(1+exp((30.*mV-V+VT)/(5.*mV)))/ms*h : 1
alpha_m = 0.32*(mV**-1)*4*mV/exprel((13*mV-(V-VSm)+VT)/(4*mV))/ms : Hz
beta_m = 0.28*(mV**-1)*5*mV/exprel(((V-VSm)-VT-40*mV)/(5*mV))/ms : Hz
alpha_h = 0.128*exp((17*mV-(V-VSh)+VT)/(18*mV))/ms : Hz
beta_h = 4./(1+exp((40*mV-(V-VSh)+VT)/(5*mV)))/ms : Hz
alpha_n = 0.032*(mV**-1)*5*mV/exprel((15*mV-V+VT)/(5*mV))/ms : Hz
beta_n = .5*exp((10*mV-V+VT)/(40*mV))/ms : Hz
noise = sigma*(2*gl/Cm)**.5*randn()/sqrt(dt) :volt/second (constant over dt)
I : amp
I_Nap = -g_nap/(1+exp((-51.*mV-V)/(4.*mV)))*hp*(V-ENa) : amp
I_syn =  I_ampa + I_nmda : amp
I_ampa = g_ampa*eta*(V-E_ampa)*s_ampa : amp
I_nmda = g_nmda*eta*(V-E_nmda)*s_nmda_tot/(1+exp(-0.062*V/mV)/3.57) : amp
s_nmda_tot :1
ds_ampa/dt = -s_ampa/tau_ampa :1 
I_AHP = -g_AHP*eta*Ca*(V-EK) : amp
dCa/dt = - Ca / tau_Ca : 1 
x : meter
y : meter
''')

# Make population of neurons
P = NeuronGroup(N, model=eqs, threshold='V>0*mV', reset='Ca += alpha_Ca', refractory=2 * ms, method='exponential_euler')

# Initialize neuron parameters
P.V = -39 * mV                          # approximately resting membrane potential
P.I = '(rand() -0.5)* 19 * pA'          # Make neurons heterogeneously excitable

# Position neurons on a grid
grid_dist = 45 * umeter
P.x = '(i % Nl) * grid_dist'
P.y = '(i // Nl) * grid_dist'

# synapse model
eqs_synapsmodel = '''
s_nmda_tot_post = w * S * x_d * s_nmda  :1 (summed)
ds_nmda/dt = -s_nmda/(taus_nmda)+alpha_nmda*x_nmda*(1-s_nmda) : 1 (clock-driven)
dx_nmda/dt = -x_nmda/(taux_nmda) :1 (clock-driven)
dx_d/dt = (1-x_d)/tau_d :1 (clock-driven)
w : 1
'''

eqs_onpre = '''
x_nmda += 1
x_d *= (1-U)
s_ampa += w * S * x_d 
'''

# Make synapses
Conn = Synapses(P, P, model=eqs_synapsmodel, on_pre=eqs_onpre, method='euler')

# connect neurons based on topology preferences
if smallworld:
    G = nx.watts_strogatz_graph(n=N, k=N // 400, p=0.5, seed=1)                 # create a network
    C = nx.adjacency_matrix(G, nodelist=None, weight=1)                         # create sparse adjacency matrix
    C = scipy.sparse.dia_matrix.toarray(C)                                      # make normal numpy array
    sources, targets = C.nonzero()                                              # create source and target indices

    Conn.connect(i=sources, j=targets)  # connect using source and target indices

if sparseconnectivity:
    Conn.connect(p=prob)

if fullconnectivity:
    Conn.connect()

if distancedependance:
    Conn.connect('i!=j', p='alpha*exp(-sqrt((x_post-x_pre)**2+(y_post-y_pre)**2)/(2*mmeter))')

if distributed:
    Conn.w[:] = 'clip(1.+sd*randn(), 0, 2)'
else:
    Conn.w[:] = 1

Conn.x_d[:] = 1

if DistDelays:
    Vmax = (sqrt(Nl ** 2 + Nl ** 2) * grid_dist) / Maxdelay
    Conn.delay = '(sqrt((x_pre - x_post)**2 + (y_pre - y_post)**2))/Vmax'
else:
    Conn.delay = 0*ms

# SET UP MONITORS AND RUN
recordstring = ['V']
if synapseplot:
    recordstring.extend(['I_ampa', 'I_nmda'])
    if STDplot:
        recordstring.append('x_d')
else:
    if STDplot:
        recordstring.extend(['I_ampa', 'I_nmda', 'x_d'])

if adaptationplot:
    recordstring.append('I_A'
                        'HP')

dt2 = defaultclock.dt                                           # Allows for chanching the timestep of recording

trace = StateMonitor(P, recordstring, record=True, dt=dt2)

spikes = SpikeMonitor(P)
run(simtime, report='text', profile=True)                       # run


if topologyplot or topologyelecplot:                            # obtain adjacency matrix
    # store connectivity matrix
    Adj = np.full((N, N), 0)
    Adj[Conn.i[:], Conn.j[:]] = Conn.w[:]

# PLOT
if voltplot:
    plt.figure(dpi=200)
    plot(trace.t / second, trace[2].V / mV, 'k', linewidth=0.7)
    xlabel('time (s)')
    ylabel('Membrane Potential of a neurons (mV)')
    #xlim([6,7.5])
    savefig(outputdir + simname + 'voltsingleneuron.png')
    show()

if rasterplot:
    plt.figure(dpi=200)
    plt.plot(spikes.t / second, spikes.i, '.k', ms=0.7)
    xlabel('time (s)')
    ylabel('neuron index')
    savefig(outputdir + simname + 'brianraster.png')
    show()

# set up a filter to filter the voltage signal
fs = 1 / (dt2 / second)
fc = 100                                            # Cut-off frequency of the filter
w = fc / (fs / 2)                                   # Normalize the frequency
b, a = signal.butter(5, w, 'high')
voltagetraces = zeros((12,len(trace.t)))

if electrodeplot:
    elec_grid_dist = (grid_dist * (Nl - 1)) / 4     # electrode grid size (there are 12 electrodes)
    elec_range = 3 * grid_dist                      # measurement range of each electrode
    comp_dist = ((Nl - 1) * grid_dist - elec_grid_dist * 3) / 2

    try:
        elecranges = pickle.load(open("elecranges.dat", "rb"))
    except FileNotFoundError:
        #Find the neurons that need to be measures, outcomment the first time run.
        elecranges = np.zeros((16, 40), dtype=int)
        for i in range(len(elecranges)):
            templist = list()
            for j in range(N):
                x_electrode = i % 4 * elec_grid_dist + comp_dist
                y_electrode = i // 4 * elec_grid_dist + comp_dist
                if sqrt((x_electrode - P[j].x) ** 2 + (y_electrode - P[j].y) ** 2) < elec_range:
                    templist.append(j)
                elecranges[i, 0:len(templist)] = templist
            templist = None
        pickle.dump(elecranges, open("elecranges.dat", "wb"))

    plt.figure(figsize=(10, 6), dpi=300)
    k = 0
    MaxAPs = len(spikes.t)
    APs = np.array([0,0])
    for i in [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14]:
        templist = elecranges[i, :]
        x_electrode = i % 4 * elec_grid_dist + comp_dist
        y_electrode = i // 4 * elec_grid_dist + comp_dist
        templist = [j for j in templist if j != 0]
        Voltage = np.zeros(len(trace.V[0]))
        for l in range(len(templist)):
            Voltage += trace[templist[l]].V / mV * 1 / (
                        sqrt((x_electrode - P.x[templist[l]]) ** 2 + (y_electrode - P.y[templist[l]]) ** 2) / (
                            grid_dist * 0.2))
        Voltage = Voltage - mean(Voltage)
        Voltagefilt = signal.filtfilt(b, a, Voltage)  # high pass filter
        threshold = 4 * np.sqrt(np.mean(Voltagefilt ** 2))      #threshold to detect APs
        APstemp, _ = find_peaks(abs(Voltagefilt), height=threshold)
        for j in range(len(APstemp)):
            APs = np.append(APs, [k,APstemp[j]])
        voltagetraces[k,:] = Voltagefilt
        plt.plot(trace.t / second, Voltagefilt + k * 100, linewidth=0.75, color='black')
        templist = None
        k += 1
    xlabel('time (s)', fontsize=15)
    plt.tick_params(
        axis='y',
        which='both',
        left=False,
        right=False,
        labelleft=False)
    #xlim([5.5,9])
    tight_layout()
    plt.savefig(outputdir + simname + 'elecvolt.png')
    show()

if rasterlecplot:
    APsres = APs.reshape(len(APs) // 2, 2)
    figure(figsize=(10, 2.2), dpi=300)
    plt.plot(APsres[:, 1] * dt2, APsres[:, 0], 'k|', ms=9, alpha=.5)
    plt.hlines([-0.3, 0.7, 1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 7.7, 8.7, 9.7, 10.7], -3, simtime/second+3, colors='black',
               linewidths=0.5)
    xlabel('Time (second)')
    ylabel('Spike times per electrode')
    xlim([transient/second-1, simtime/second+1])
    axis("off")
    tight_layout()
    savefig(outputdir + simname + 'ownraster.png')
    show()

if STDplot:
    fig, axs = plt.subplots(4, 1, sharex='all', dpi=200, figsize=[8, 6.5])
    axs[0].plot(spikes.t / second, spikes.i, '.k', ms=1)
    axs[0].set_ylabel('Neuron index')

    axs[1].plot(trace.t / second, sum(trace.x_d, axis=0) / len(trace.x_d) * U, 'k')
    axs[1].set_ylabel('Short-Term depression')
    axs[1].grid(visible=True, linestyle=':')

    axs[2].plot(trace.t / second, sum(trace.I_nmda, axis=0) / len(trace.I_nmda) * -1 / pA, 'k')
    axs[2].set_ylabel('NMDA current (pA)')
    axs[2].grid(visible=True, linestyle=':')

    axs[3].plot(trace.t / second, sum(trace.I_ampa, axis=0) / len(trace.I_ampa) * -1 / pA, 'k')
    axs[3].set_ylabel('AMPA current (pA)')
    axs[3].set_xlabel('time (s)')
    axs[3].grid(visible=True, linestyle=':')
    fig.tight_layout()
    savefig(outputdir + simname + 'STDplot.png', dpi=300)
    show()

if synapseplot:
    fig, axs = plt.subplots(2, 1, sharex='all', dpi=200, figsize=[8, 5])
    axs[0].plot(trace.t / second, sum(trace.I_nmda, axis=0) / len(trace.I_nmda) * -1 / pA, 'k')
    axs[0].set_ylabel('NMDA current (pA)')
    axs[0].grid(visible=True, linestyle=':')

    axs[1].plot(trace.t / second, sum(trace.I_ampa, axis=0) / len(trace.I_ampa) * -1 / pA, 'k')
    axs[1].set_ylabel('AMPA current (pA)')
    axs[1].set_xlabel('time (s)')
    axs[1].grid(visible=True, linestyle=':')
    fig.tight_layout()
    #xlim([6,7])
    savefig(outputdir + simname + 'EPSPs.png', dpi=300)
    show()

if adaptationplot:
    plot(trace.t / ms, sum(trace.I_AHP, axis=0) / len(trace.I_AHP)/pA)
    xlabel('t (ms)')
    ylabel('I_AHP (pA)')
    show()

if topologyplot:
    Adj[0:300, :] = 0
    Adj[301:6250000, :] = 0
    Network = nx.convert_matrix.from_numpy_matrix(Adj)  # make graph of adjacency matrix

    nodess = list(range(0, N))
    position = [(P.x[i] / grid_dist, P.y[i] / grid_dist) for i in range(N)]
    pos = {nodess[i]: position[i] for i in range(len(nodess))}  # make position argument

    plt.figure(figsize=(10, 10), dpi=200)
    nx.draw(Network, pos, node_size=30000 / N)
    savefig(outputdir + simname + 'topology.png')
    show()

if topologyelecplot:
    elecNetwork = nx.convert_matrix.from_numpy_matrix(np.zeros((12, 12)))
    x_elec = [(i % 4 * elec_grid_dist + comp_dist) / grid_dist for i in [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14]]
    y_elec = [(i // 4 * elec_grid_dist + comp_dist) / grid_dist for i in [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14]]
    elecnodess = list(range(0, 12))
    elecposition = [(x_elec[i], y_elec[i]) for i in range(12)]
    elecpos = {elecnodess[j]: elecposition[j] for j in range(12)}

    plt.figure(figsize=(10, 10), dpi=200)
    nx.draw(Network, pos, node_size=30000 / N, edgelist=[], node_color='black')
    nx.draw(elecNetwork, elecpos, node_size= 450, node_color='chocolate')
    savefig(outputdir + simname + 'topologyelec.png')
    show()

if singleelecplot:
    figure(dpi=300)
    plot(trace.t / second, voltagetraces[0, :], 'k', linewidth=0.75)
    xlabel('time (s)')
    ylabel('Voltage (mV)')
    #xlim([6.5, 8])
    savefig(outputdir + simname + 'singleelec.png')
    show()

if not rasterlecplot:
    APsres = APs.reshape(len(APs)//2, 2)

#Export data to .mat to analyse in MATLAB identical to experimental data 
if not rasterlecplot:
    APsres = APs.reshape(len(APs)//2, 2)

#pickle.dump(APsres, open("APs_Lino1.dat", "wb"))

tempdic = {"APs": APsres, "label": "Control"}
savemat(outputdir + simname + ".mat", tempdic)
