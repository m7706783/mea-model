# Multiple runs of the model as described in NetworkModel.py
# Makes multiple networks with different random number realisations
# Saves the spike times of the simulations in ".mat" files to be analyzed in MATLAB

# Written by Nina Doorn
# last edit: 26-07-2022
# n.doorn-1@utwente.nl

# IMPORT LIBRARIES
from brian2 import devices
import scipy
from scipy import signal
import os
from numpy import *
from numpy import random
from brian2 import *
import networkx as nx
import matplotlib.pyplot as plt
import pickle
import sys

from scipy.signal import find_peaks
from scipy.io import savemat

# # Skip C99 support check
import brian2.codegen.cpp_prefs
brian2.codegen.cpp_prefs._compiler_supports_c99 = True

# SET PARAMETERS
outputdir = '/home/Nina/Documents/M_Thesis/DS_project/MATLABoutput'

simtime = 650 * second              # simulation time (50 ms transient)
numruns = 8                         # number of runs

# choose topology
smallworld = False                  # Small world topology
sparseconnectivity = True           # Random sparse connectivity with connection probability prob
prob = 0.3
fullconnectivity = False            # All neurons are connected
distancedependance = False          # If neurons are connected depends on the distance. Neurons have a
alpha = 0.025                       # connection probability alpha

distributed = True                  # Choose if the weights should have a distribution with standard deviation sd
sd = 0.7
DistDelays = True                   # Choose if the delays should be distant dependent
Maxdelay = 25*ms

# PARAMETERS
Nl = 10
N = Nl * Nl                         # number of neurons

# neuron parameters
T = 37                              # temperature
T2 = 37+273.15
area = 300*umetre**2
Cm = (1*ufarad*cm**-2) * area
El = (-39.2 * mV)/(37+273.15)*T2
EK = (-80 * mV)/(37+273.15)*T2
ENa = (70 * mV)/(37+273.15)*T2
g_na = (50*msiemens*cm**-2) * area  #maximal conductance of potassium channels
g_kd = (5*msiemens*cm**-2) * area
gl = (0.3*msiemens*cm**-2) * area
VT = -30.4*mV                       # alters firing threshold of neurons
sigma = 4.1 * mV                    # noisy voltage fluctuations SD

#temperature parameters
phi = 3**((T-37)/10)
B = 0 #0.05853
eta = 1+B*(T-37)

# Adaptation parameters
Adaptation = True                   # Spike frequency adaptation using afterhyperolarization current
E_AHP = -80 * mV                    # nernst potential of afterhyperpolarization current
g_AHP = 10 * nS                     # maximum calcium conductance
tau_Ca = 6000 * ms                  # recovery time constant of Calcium conductance
if Adaptation:
    alpha_Ca = 0.00035              # strength of the adaptation
else:
    alpha_Ca = 0

# synapse parameters
delta = 0                           # increase to alter NMDAR/AMPAR ratio
g_ampa = ((104.0 * (1 + 10 * delta)) / 1000) * 2.7 * nS  # maximal conductance
g_nmda = ((327 * (1 - delta)) / 1000) * 0.3 * nS
E_ampa = 0 * mV                     # Nernst potentials of synaptic channels
E_nmda = 0 * mV
tau_ampa = 2 * ms                   # recovery time constant of ampa conductance
taus_nmda = 100 * ms                # rise time constant of nmda conductance
taux_nmda = 2 * ms                  # decay time constant of nmda conductance
alpha_nmda = 0.5 * kHz

S = 1.65                            # weight scaling parameter (decrease with increasing N)

# Short-Term Depression parameters
tau_d = 813 * ms                    # Recovery time constant of depression
U = 0.015                           # Magnitude of depression

# sodium channel modulation parameters
gam_na = 1                          # modulates maximal conductance
gam_tm = 1                          # modulates both rate constants of m gate
gam_th = 1                          # modulates both rate constants of h gate
gam_am = 1                          # modulates activation rate of m gate
gam_bm = 1                          # modulates deactivation of m gate
gam_ah = 1                          # modulates activation of h gate
gam_bh = 1                          # modulates deactivation of h gate
VSm = 0*mV                          # shifts voltage sensitivity of rate constants of m gate
VSh = 0*mV                          # shifts voltage sensitivity of rate constants of h gate

gam_nap = 0                          #modulates what fraction (0-1) of sodium channels becomes persistent

max_g_nap = (0*msiemens*cm**-2) * area
g_nap = gam_nap*max_g_nap
gam_na = (1-gam_nap)*gam_na

# EQUATIONS
# Neuron model
eqs = Equations('''
dV/dt = noise + (-gl*eta*(V-El)-gam_na*g_na*eta*(m*m*m)*h*(V-ENa)-g_kd*eta*(n*n*n*n)*(V-EK)+I-I_syn+I_AHP+I_Nap)/Cm : volt
dm/dt = phi*gam_tm*(gam_am*alpha_m*(1-m)-gam_bm*beta_m*m) : 1
dh/dt = phi*gam_th*(gam_ah*alpha_h*(1-h)-gam_bh*beta_h*h) : 1
dn/dt = phi*(alpha_n*(1-n)-beta_n*n) : 1
dhp/dt = 0.128*exp((17.*mV-V+VT)/(18.*mV))/ms*(1.-hp)-4./(1+exp((30.*mV-V+VT)/(5.*mV)))/ms*h : 1
alpha_m = 0.32*(mV**-1)*4*mV/exprel((13*mV-(V-VSm)+VT)/(4*mV))/ms : Hz
beta_m = 0.28*(mV**-1)*5*mV/exprel(((V-VSm)-VT-40*mV)/(5*mV))/ms : Hz
alpha_h = 0.128*exp((17*mV-(V-VSh)+VT)/(18*mV))/ms : Hz
beta_h = 4./(1+exp((40*mV-(V-VSh)+VT)/(5*mV)))/ms : Hz
alpha_n = 0.032*(mV**-1)*5*mV/exprel((15*mV-V+VT)/(5*mV))/ms : Hz
beta_n = .5*exp((10*mV-V+VT)/(40*mV))/ms : Hz
noise = sigma*(2*gl/Cm)**.5*randn()/sqrt(dt) :volt/second (constant over dt)
I : amp
I_Nap = -g_nap/(1+exp((-51.*mV-V)/(4.*mV)))*hp*(V-ENa) : amp
I_syn =  I_ampa + I_nmda : amp
I_ampa = g_ampa*eta*(V-E_ampa)*s_ampa : amp
I_nmda = g_nmda*eta*(V-E_nmda)*s_nmda_tot/(1+exp(-0.062*V/mV)/3.57) : amp
s_nmda_tot :1
ds_ampa/dt = -s_ampa/tau_ampa :1 
I_AHP = -g_AHP*eta*Ca*(V-EK) : amp
dCa/dt = - Ca / tau_Ca : 1 
x : meter
y : meter
''')

# synapse model
eqs_synapsmodel = '''
s_nmda_tot_post = w * S * 0.35 * s_nmda  :1 (summed)
ds_nmda/dt = -s_nmda/(taus_nmda)+alpha_nmda*x_nmda*(1-s_nmda) : 1 (clock-driven)
dx_nmda/dt = -x_nmda/(taux_nmda) :1 (clock-driven)
dx_d/dt = (1-x_d)/tau_d :1 (clock-driven)
w : 1
'''

eqs_onpre = '''
x_nmda += 1
x_d *= (1-U)
s_ampa += w * S * x_d 
'''

for o in range(numruns):                        # Iterate over all the runs
    if o!=0:                                    # delete info from the previous run
        del P
        del Conn

    devices.device.seed(o)

    # Make population of neurons
    P = NeuronGroup(N, model=eqs, threshold='V>0*mV', reset='Ca += alpha_Ca', refractory=2 * ms, method='exponential_euler')

    # Initialize neurons
    P.V = -39 * mV
    P.I = '(rand() -0.5)* 19 * pA'              # Make neurons heterogeneously sensitive

    # position neurons on a grid
    grid_dist = 45 * umeter
    P.x = '(i % Nl) * grid_dist'
    P.y = '(i // Nl) * grid_dist'

    # make synapses
    Conn = Synapses(P, P, model=eqs_synapsmodel, on_pre=eqs_onpre, method='euler')

    # connect neurons based on topology preferences
    if smallworld:
        G = nx.watts_strogatz_graph(n=N, k=N // 400, p=0.5, seed=1)  # create a network
        C = nx.adjacency_matrix(G, nodelist=None, weight=1)  # create sparse adjacency matrix
        C = scipy.sparse.dia_matrix.toarray(C)  # make normal numpy array
        sources, targets = C.nonzero()  # create source and target indices

        Conn.connect(i=sources, j=targets)  # connect using source and target indices

    if sparseconnectivity:
        Conn.connect(p=prob)

    if fullconnectivity:
        Conn.connect()

    if distancedependance:
        Conn.connect('i!=j', p='alpha*exp(-sqrt((x_post-x_pre)**2+(y_post-y_pre)**2)/(2*mmeter))')

    if distributed:
        Conn.w[:] = clip(numpy.random.normal(1, sd, (len(Conn))), 0, 2)
    else:
        Conn.w[:] = 1

    Conn.x_d[:] = 1

    if DistDelays:
        Vmax = (sqrt(Nl ** 2 + Nl ** 2) * grid_dist) / Maxdelay
        Conn.delay = '(sqrt((x_pre - x_post)**2 + (y_pre - y_post)**2))/Vmax'
    else:
        Conn.delay = 0*ms

    # set up monitors and run
    dt2 = defaultclock.dt

    trace = StateMonitor(P, 'V', record=True, dt=dt2)

    spikes = SpikeMonitor(P)
    run(simtime, report='text', profile=True)

    # set up a filter to filter the voltage signal
    fs = 1 / (dt2 / second)
    fc = 100  # Cut-off frequency of the filter
    w = fc / (fs / 2)  # Normalize the frequency
    b, a = signal.butter(5, w, 'high')

    elec_grid_dist = (grid_dist * (Nl - 1)) / 4  # electrode grid size (there are 12 electrodes)
    elec_range = 3 * grid_dist  # measurement range of each electrode
    comp_dist = ((Nl - 1) * grid_dist - elec_grid_dist * 3) / 2

    elecranges = pickle.load(open("elecranges.dat", "rb"))
    k = 0
    MaxAPs = len(spikes.t)
    APs = np.array([0,0])
    for i in [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14]:
        templist = elecranges[i, :]
        x_electrode = i % 4 * elec_grid_dist + comp_dist
        y_electrode = i // 4 * elec_grid_dist + comp_dist
        templist = [j for j in templist if j != 0]
        Voltage = np.zeros(len(trace.V[0]))
        for l in range(len(templist)):
            Voltage += trace[templist[l]].V / mV * 1 / (
                        sqrt((x_electrode - P.x[templist[l]]) ** 2 + (y_electrode - P.y[templist[l]]) ** 2) / (
                            grid_dist * 0.2))
        Voltage = Voltage - mean(Voltage)
        Voltagefilt = signal.filtfilt(b, a, Voltage)  # high pass filter
        threshold = 4 * np.sqrt(np.mean(Voltagefilt ** 2))      #threshold to detect APs
        APstemp, _ = find_peaks(abs(Voltagefilt), height=threshold)
        for j in range(len(APstemp)):
            APs = np.append(APs, [k,APstemp[j]])
        templist = None
        k += 1

    # Export data to .mat to analyse in MATLAB
    APsres = APs.reshape(len(APs)//2, 2)

    tempdic = {"APs": APsres, "label": "Control"}
    savetitle = '/Control_run_' + str(o) + '.mat'
    savemat(outputdir + savetitle, tempdic)
