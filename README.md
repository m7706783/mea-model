Scripts to run simulations with the model described in "An in silico and in vitro human neuronal network model reveal cellular mechanisms beyond NaV1.1 underlying Dravet Syndrome." 

Consists of:
 - Analysis_MATLAB: contains the scripts used for the analysis of both simulated and experimental data to compute network bursting features and detect sEPSCs. 
 - Model_Python: The python code to run simulations with the in silico model as described in the paper. 

Paper can be found here: https://www.sciencedirect.com/science/article/pii/S2213671123002321?via%3Dihub
